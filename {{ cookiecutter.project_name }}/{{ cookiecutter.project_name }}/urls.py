from django.contrib import admin
from django.urls import path, re_path, include
from rest_framework_jwt.views import ( 
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token
)

urlpatterns = [
    path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    re_path(r'^auth/refresh/?$', refresh_jwt_token),
    re_path(r'^auth/verify/?$', verify_jwt_token),
    re_path(r'^auth/?$', obtain_jwt_token),
]
