import os
from .common import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_env_var("DJANGO_SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ["{{ cookiecutter.fqdn }}"]

# Enforce HTTPS/SSL
SECURE_SSL_HOST = True

# Always send cookies through HTTPS
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

{% if cookiecutter.use_hsts == "yes" %}
# Use HTTP Strict Transport Security.
# Begin with a max-age of 5 minutes, as recommended in
# https://hstspreload.org/#deployment-recommendations
SECURE_HSTS_SECONDS = 300
{% endif %}
